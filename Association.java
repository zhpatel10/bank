package bank;

public class Association {

    public static void main(String[] args) {
        Bank b1= new Bank("Central Bank of India");

        Employee emp1=new Employee("Zalak Patel");

        System.out.println(emp1.getEmpName() + " is employee of " + b1.getBankName() + " Bank ");
    }
}
